# -*- coding: utf-8 -*-
"""
Created on Thu Aug 30 09:25:24 2018

@author: Shawn Fox
"""
import time
import random

print('Welcome To Hangman')

user = input('Enter your Name: ')
print('Hello' ,user)


ready = input('Are You Ready to Play: ')
print('Lets get started ')



#The while loop allows for the game to be played again if the user inputs yes
playagain = "yes"
while playagain == "yes":
    #k is a variable that records the guesses
    k=[]
    time.sleep(1)
     
    dif = input('Choose a difficulty 1-3: ')
    
    if (dif == "1"): #telling txt file to pull from
        with open('words.txt', "r") as f: #pulls words from txt file
            bank = f.readlines()
        for i in range(len(bank)):#loops through word bank
            bank[i] = bank[i].replace("\n", "")#takes new line and replaces
            #with nothing
            #print(bank[i]) this print function was used to check that the 
            #txt file was being opened properly
        f.close()
    
    if (dif == "2"):
        with open('words2.txt', "r") as f: 
            bank = f.readlines()
        for i in range(len(bank)): 
            bank[i] = bank[i].replace("\n", "")
        f.close()
        
    if (dif == "3"):
        with open('words3.txt', "r") as f: 
            bank = f.readlines()
        for i in range(len(bank)):
            bank[i] = bank[i].replace("\n", "")
        f.close()
    
    
    lives = 10 #sets the number of turns
    #print('you have', lives, 'lives')
    
    #This pulls a random word from the txt file
    word = bank[random.randint(0, len(bank)-1)]
    userword = []
    for i in range(len(word)):
        userword.append('_')
   
    #j is a variable that identifies the # of letters correct in the word
    j=0
    while lives > 0:        #starts a while loop for turns greater than zero
        guess = input("Choose a letter: ").lower() 
        #calls to the player for an input ensures input is lowercase
         
        
        if guess in word:#indicates if the guess is in the word
            if guess in k: #Prevents lives being taken for repeat guess 
                print("Letter already guessed ")
            else:
                k.append(guess)#adds guess to the k
                print('Letter is in word! ')
                for i in range(len(word)):
                    if list(word)[i] == guess:
                        userword[i]=guess
                        j+=1#adding one to the variable j
                print(" ".join(userword))
        else:
            if guess in k:
                print("Letter already guessed ")
            else:
                k.append(guess)
                print('WRONG!!! ')  
                lives -=1  #takes a life away after wrong guess
            
        # if statement uses j to determine if the word has been properly 
        #guessed
        if j == len(word):
            print("You have Won!!!")
            break
        
        #if the guess is not in the word this statement outputs the 
        #amount of lives remaining
        if guess not in word:
                print("You have", +lives, 'lives remaining')
       
        #if the number of lives remaining is equal to zero the game is over
        if lives == 0:
            print("GAME OVER")
            break
        #the breaks exit the while loop for lives >0 and enters the play again
        #loop allowing the player to play again
    playagain = input("Would you like to play again (yes or no): ")