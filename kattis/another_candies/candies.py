# -*- coding: utf-8 -*-
"""
Created on Sun Oct 21 12:22:43 2018

@author: Shawn Fox
"""
test = int(input())
input()
num_child = int(input())
total = 0
for i in range (test):
    for j in range (num_child):
        total = int(input()) + total
    print('YES' if total%num_child == 0 else 'NO')
    if i != test-1:
        input()
        num_child = int(input())
        total = 0