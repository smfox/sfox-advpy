# -*- coding: utf-8 -*-
"""
Created on Mon Oct 22 14:19:31 2018

@author: Shawn Fox
"""
import sys

def new_price(price, bill):
    base = pow(10,bill)
    a = price/base
    b = round(a, 0)
    c = int(b*10**bill)
    return c

def test():
    assert new_price(2000001, 1) == 2000000
    assert new_price(10, 0) == 10
    assert new_price(10, 3) == 0
    assert new_price(27272727227272, 2) == 27272727227300
    print('ALL TEST PASS')
if __name__ == '__main__':
    if(len(sys.argv) > 1 and sys.argv[1] == 'test'):
        test()
    else:
        price, bill = map(int, input().split())
        print(new_price(price, bill))
        