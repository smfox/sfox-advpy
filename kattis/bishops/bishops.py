# -*- coding: utf-8 -*-
"""
Created on Fri Oct 19 13:36:16 2018

@author: Shawn Fox
"""

import sys

line = sys.stdin.readlines()

for i in range(len(line)):
    line[i] = int(line[i])
    
    if line[i] == 1:
        print('1')
    else:
        print(2*line[i]-2)
    