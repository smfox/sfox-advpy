# -*- coding: utf-8 -*-
"""
Created on Sun Oct 21 10:21:01 2018

@author: Shawn Fox
"""
import math
from math import pi

a,b = map(int,input().split())
#print(a,b)

if (b >= 0 and b <= 180):
    print('safe')     
else: 
    radians = abs(b*pi/180)
    d = int(abs(a/math.sin(radians)))
    print(d) 