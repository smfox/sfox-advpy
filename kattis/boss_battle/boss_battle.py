# -*- coding: utf-8 -*-
"""
Created on Fri Oct 19 12:06:31 2018

@author: Shawn Fox
"""
import sys
#num_pillars = int(input())
#bombs = 1
def num_bombs(num_pillars):
    if num_pillars <= 2:
        bombs = 1
        
    else:
        bombs = num_pillars -2 
    return bombs

def test():
    assert num_bombs(2) == 1
    assert num_bombs(8) == 6
    assert num_bombs(6) == 4
    assert num_bombs(0) == 1
    print('Test PASS')
if __name__ == '__main__':
    if (len(sys.argv) > 1 and sys.argv[1] == 'test'):
        test()
    else:
        num_pillars = int(input())
        print(num_bombs(num_pillars))